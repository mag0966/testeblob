package com.maginfo.testeblob.model;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * 
 * @author Marcos
 * 
 * @version 1.00/2016 - Classe para conex�o FIREBIRD
 * jdbc:firebirdsql://localhost:3050/C:/database/MFREIRE.FBD
 */
public class Conexao {
	// Definir vari�vel e conte�do da localiza��o do banco de dados
	private String databaseUrl = "jdbc:firebirdsql://localhost:3050/C:/database/MARA2015.FBD";
	private String usuario = "SYSDBA";
	private String senha = "masterkey";
	public Conexao() {
		super();
	}
	// M�todo para estabelecer a conex�o
	public Connection getConexao() throws ClassNotFoundException, SQLException {
		// Define classe jdbc para a conex�o
		Class.forName("org.firebirdsql.jdbc.FBDriver");
		Connection connection = java.sql.DriverManager.getConnection(this.databaseUrl,this.usuario,this.senha);
		return connection;
	}
}
