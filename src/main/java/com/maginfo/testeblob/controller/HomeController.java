package com.maginfo.testeblob.controller;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.sql.rowset.serial.SerialBlob;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.maginfo.testeblob.model.Conexao;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	// Defini��o da vari�vel de data
	private String dataFormatada = new SimpleDateFormat("dd/dd/yyyy").format(new Date());
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		ModelAndView mv1 = new ModelAndView();
		mv1.setViewName("home");
		mv1.addObject("serverTime", this.dataFormatada);
		return "home";
	}
	@RequestMapping(value="registrobranco",method=RequestMethod.GET)
	public ModelAndView registroBranco() throws ClassNotFoundException, SQLException {
		ModelAndView mv1 = new ModelAndView();
		mv1.setViewName("home");
		mv1.addObject("serverTime",this.dataFormatada);
		// Vari�vel de retorno da inclus�o do novo registro
		String operacao = "OK";
		String mensagem = "";
		// Executar a inclus�o do cadastro em branco
		Conexao conexao = new Conexao();
		Connection connection = conexao.getConexao();
		String sql = "INSERT INTO TESTE (CADASTRO) VALUES (1);";
		PreparedStatement pstm = connection.prepareStatement(sql);
		try {
			pstm.executeQuery();
			mensagem  = "O registro numero 1 foi criado com sucesso.";
		} catch(Exception e) {
			operacao = "NAO";
			mensagem = "O registro n�mero 1 j� existe no arquivo.";
		}
		mv1.addObject("operacao",operacao);
		mv1.addObject("mensagem",mensagem);
		return mv1;
	}
	//
	// M�todo para grava��o de uma imagem no campo BLOB
	//
	@RequestMapping(value="gravaimagem",method=RequestMethod.GET)
	public ModelAndView gravaImagem() throws ClassNotFoundException, SQLException {
		ModelAndView mv1 = new ModelAndView();
		// Vari�vel para controle de grava��o
		Integer resultado = 0;
		mv1.setViewName("home");
		String operacao = "OK";
		String mensagem = "Registro BLOB atualizado com sucesso !!!";
		// Criar uma conexao
		Conexao conexao = new Conexao();
		Connection connection = conexao.getConexao();
		// Cria objeto para providenciar o update no arquivo
		String sql = "UPDATE TESTE SET IMAGEM = ? WHERE CADASTRO = 1";
		PreparedStatement pstm = connection.prepareStatement(sql);
		try {
			// Um teste para a tentativa de gravar-se uma String no BLOB
			String wteste = " Esta linha de texto ser� utilizada para testar a leitura de um arquivo" 
					       +"\r\n"
						   +" gravando o mesmo em um campo do tipo BLOB no banco de dados Firebird 2.1."
						   +"\r\n"
						   +" ESTE VEIO DE UMA STRING: "
						   +"\r\n"
						   +"Este teste dever� ser efetuado, at� que o procedimento de grava��o e lei-"
						   +"\r\n"
						   +"tura, ser� realizado de maneira a que:"
						   +"\r\n"
                           +"1) O arquivo que se grava, seja express�o id�ntica do arquivo lido."
                           +"\r\n"
                           +"2) O arquivo re-lido do campo BLOB, seja exatamente igual ao arquivo"
                           +"\r\n"
                           +" originalmente lido.";
             Blob blob = new SerialBlob(wteste.getBytes());
			 pstm.setBlob(1, blob);
			 resultado = pstm.executeUpdate();
			
			// Esta parte esta funcionando perfeitamente para ler e gravar
			// um arquivo .txt no campo BLOB do banco de dados.
			// InputStream bin = new FileInputStream("c:\\Teste\\Texto.txt");
			// pstm.setBlob(1, bin);
			// resultado = pstm.executeUpdate();
             
		} catch(Exception e) {
			mensagem = "Excessao: "+e.getMessage();
		}
		mv1.addObject("operacao",operacao);
		mv1.addObject("mensagem",mensagem);
		mv1.addObject("registros",resultado);
		connection.close();
		return mv1;
	}
}
