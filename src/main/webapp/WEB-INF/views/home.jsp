<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<head>
	<title>Testes com campo blob</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style.css">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
</head>
<body>
	<hr>
	<h4>Execu��o de testes com campos do tipo BLOB - Guarulhos, ${serverTime}</h4>
	<hr>
	<a href="#" class="btn btn-primary btn-sm" id="id_btnTestar" onclick="minhaFuncao()"   >Iniciar os testes</a>
	<a href="${pageContext.request.contextPath}/registrobranco" class="btn btn-default btn-sm">Criar registro (1) em branco </a>
	<a href="${pageContext.request.contextPath}/gravaimagem" class="btn btn-warning btn-sm">Gravar BLOB no banco</a>
	<c:if test="${operacao != null}">
		<script type="text/javascript">
			alert('${mensagem}'); 		
		</script>
	</c:if>

	<!-- Defini��o de script para acionar o modal de novo telefone -->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>	
	<!-- In�cio de programa��o javascript e ajax -->	
	
	<script type="text/javascript">
		function minhaFuncao() {
			alert("Estou pronto para come�ar, � s� fazer o processo !!!");
		}	
	</script>
</body>
</html>
